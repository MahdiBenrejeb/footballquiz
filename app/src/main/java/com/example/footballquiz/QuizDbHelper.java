package com.example.footballquiz;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.footballquiz.QuizContract.*;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class QuizDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "FootballQuiz.db";
    private static final int DATABASE_VERSION = 1;

    private SQLiteDatabase db ;

    public QuizDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.db = db;
        final String SQL_CREATE_QUESTIONS_TABLE ="CREATE TABLE " +
                Questions_Table.TABLE_NAME + " ( " +
                Questions_Table._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                Questions_Table.COLUMN_QUESTION + " TEXT, " +
                Questions_Table.COLUMN_OPTION1 + " TEXT, " +
                Questions_Table.COLUMN_OPTION2 + " TEXT, " +
                Questions_Table.COLUMN_OPTION3 + " TEXT, " +
                Questions_Table.COLUMN_ANSWER_NR + " INTEGER" +
                ")";

        db.execSQL(SQL_CREATE_QUESTIONS_TABLE);
        fillQuestionsTable();

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + Questions_Table.TABLE_NAME);
    onCreate(db);
    }
    private void fillQuestionsTable(){
        Question q1 = new Question("Who is the Player with the most Ballon d'Or in football History? ", "Cristiano Ronaldo","Lionel Messi","Diego Maradona",2);
        addQuestion(q1);
        Question q2 = new Question("How many times has Brazil won the World Cup?  ", "3","4","5",3);
        addQuestion(q2);
        Question q3 = new Question("Who is the first and only manager in football history to lift 6 trophies in a single season?  ", "Jose Mourinho","Pep Guardiola","Sir Alex Fergusson",2);
        addQuestion(q3);
        Question q4 = new Question("Who is the only italian defender to ever win the \"Ballon d'or\"? ", "Alessandro Nesta","Paolo Maldini","Fabio Cannavaro",3);
        addQuestion(q4);
        Question q5 = new Question("Who is the only Player in football history to win the Champions League with 3 different clubs? ", "Clarence Seedorf","Samuel Eto'o","Cristiano Ronaldo",2);
        addQuestion(q5);
    }
    private void addQuestion(Question question){
        ContentValues cv = new ContentValues();
        cv.put(Questions_Table.COLUMN_QUESTION,question.getQuestion());
        cv.put(Questions_Table.COLUMN_OPTION1,question.getOption1());
        cv.put(Questions_Table.COLUMN_OPTION2,question.getOption2());
        cv.put(Questions_Table.COLUMN_OPTION3,question.getOption3());
        cv.put(Questions_Table.COLUMN_ANSWER_NR,question.getAnswerNr());
        db.insert(Questions_Table.TABLE_NAME,null,cv);


    }
    public List<Question> getAllQuestions(){
        List<Question> questionList = new ArrayList<>();
        db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + Questions_Table.TABLE_NAME,null);
        if(c.moveToFirst()) {
            do {
                Question question = new Question() ;
                question.setQuestion(c.getString(c.getColumnIndex(Questions_Table.COLUMN_QUESTION)));
                question.setOption1(c.getString(c.getColumnIndex(Questions_Table.COLUMN_OPTION1)));
                question.setOption2(c.getString(c.getColumnIndex(Questions_Table.COLUMN_OPTION2)));
                question.setOption3(c.getString(c.getColumnIndex(Questions_Table.COLUMN_OPTION3)));
                question.setAnswerNr(c.getInt(c.getColumnIndex(Questions_Table.COLUMN_ANSWER_NR)));
                questionList.add(question);

            } while (c.moveToNext());
        }
        c.close();
        return questionList;

    }



}
